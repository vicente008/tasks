#States
puts "States"
states_list = [
  ["READY", "Tarea Lista"],
  ["NOT READY", "Tarea Pendiente"],
]
states_list.each do |code, name|
  puts " Check #{code}" 
  if State.find_by(code:code).blank?
    State.create(code:code, name:name)
    puts "  Created code:#{code} name:#{name}" 
  end
end