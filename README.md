# README

* Ruby version : 2.7
* Rails version: 6.1.5
* Database migrations: rails db:migration
* Database initialization: rails db:seed
* Webpacker initialization: bundle exec rails webpacker:install
* How to run server: rails server
* URL Home: http://localhost:3000/tasks

