class TasksController < ApplicationController
  before_action :set_state, only: [:show, :edit, :set_state_task, :destroy]

  # GET /tasks
  # GET /tasks.json
  def index
    @tasks = Task.all
  end

  # GET /tasks/1
  # GET /tasks/1.json
  def show
  end

  # GET /tasks/new
  def new
    @task = Task.new
  end

  # GET /tasks/1/edit
  def edit
  end

  # POST /tasks
  # POST /tasks.json
  def create
    @task = Task.new(task_params)
    @task.state_id = State.find_by({code: "NOT READY"}).id
    respond_to do |format|
      if @task.save
        format.html { redirect_to tasks_url, notice: 'Tarea creada exitosamente' }
        format.json { render :show, status: :created, location: @task }
      else
        format.html { render :new }
        format.json { render json: @task.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /tasks/1
  # PATCH/PUT /tasks/1.json
  def set_state_task
    current_state = State.find(@task.state_id).code
    new_state_id = current_state == "READY" ? State.not_ready.id : State.ready.id
    @task.state_id = new_state_id
    respond_to do |format|
      if @task.save
        format.html { redirect_to tasks_url, notice: 'Tarea actualizada exitosamente' }
        format.json { render :show, status: :ok, location: @task }
      else
        format.html { render :edit }
        format.json { render json: @task.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tasks/1
  # DELETE /tasks/1.json
  def destroy
    @task.destroy
    respond_to do |format|
      format.html { redirect_to tasks_url, notice: 'Tarea eliminada exitosamente' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_state
      @task = Task.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def task_params
      params.require(:task).permit(:description, :state_id)
    end
end

