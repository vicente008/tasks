class State < ApplicationRecord
    validates :code, presence: true, uniqueness: {case_sensitive: false }

    @@state_ready = State.find_by(code: "READY")
    @@state_not_ready = State.find_by(code: "NOT READY")

    def State.ready
        @@state_ready
    end

    def State.not_ready
        @@state_not_ready
    end
end
